<?php

/**
 * Implements base_settings_form()
 */
function commerce_andreani_pane_settings_form($checkout_pane) {
  $form['commerce_andreani_shipping_info_pane_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Commerce Andreani shipping service pane'),
    '#default_value' => variable_get('commerce_andreani_shipping_info_pane_field', ''),
  );
  return $form;
}

/**
 * Shows the shipping information collected in the review checkout
 */
function commerce_andreani_pane_review($form, $form_state, $checkout_pane, $order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if($line_item_wrapper->value()->data['shipping_service']['shipping_method'] == 'andreani') {
      $shipping_info['service'] = $line_item_wrapper->value()->data['shipping_service']['display_title'];
      if($line_item_wrapper->value()->data['service_details']['commerce_andreani_store']) {
        $andreani_service_details = $line_item_wrapper->value()->data['service_details']['commerce_andreani_store'];
        $andreani_store = commerce_andreani_store_details($andreani_service_details);
        $shipping_info['name'] = $andreani_store['name'];
        $shipping_info['address'] = $andreani_store['address'];
        $shipping_info['hours'] = $andreani_store['hours'];
      }
    }
  }

  $content['andreani_shipping_info'] = array(
    '#markup' => implode('<br/>', $shipping_info),
  );

  return drupal_render($content);
}
