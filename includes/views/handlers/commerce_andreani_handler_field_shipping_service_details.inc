<?php

/**
 * Field handler to display shipping service details.
 */
class commerce_andreani_handler_field_shipping_service_details extends views_handler_field_serialized {

  function render($values) {
    $value = $values->{$this->field_alias};
    $value = (array) unserialize($value);

    if (!empty($value['service_details'])) {
      $value = $value['service_details'];

      if ($this->options['format'] == 'unserialized') {
        return check_plain(print_r($value, TRUE));
      }
      elseif ($this->options['format'] == 'key' && !empty($this->options['key'])) {
        return check_plain($value[$this->options['key']]);
      }

      return serialize($value);
    }

    // There are no service details.
    return '';
  }
}
