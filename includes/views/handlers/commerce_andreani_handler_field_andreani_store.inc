<?php

/**
 * Field handler to display the store.
 */
class commerce_andreani_handler_field_andreani_store extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['store_display'] = array('default' => 'id');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['store_display'] = array(
      '#type' => 'select',
      '#title' => t('Show'),
      '#description' => t('Show store id or store name.'),
      '#default_value' => $this->options['store_display'],
      '#options' => array(
        'id' => t('ID'),
        'name' => t('Name'),
      ),
    );
  }

  function render($values) {
    $value = $values->{$this->field_alias};
    $value = (array) unserialize($value);

    if (!empty($value['service_details']['commerce_andreani_store'])) {
      $value = $value['service_details']['commerce_andreani_store'];

      if ($this->options['store_display'] == 'name') {
        $store = commerce_andreani_store_details($value);
        $value = $store ? $store['name'] : '';
      }
      return $value;
    }

    // There is no store id in the service details.
    return '';
  }
}
