<?php

/**
 * Field handler to display an order e-mail address.
 */
class commerce_andreani_handler_field_order_weight extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();

    $options['units'] = array('default' => 'g');
    $options['show_units'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Provide the units and show_units options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $units = array();
    foreach (physical_weight_units() as $unit => $info) {
      $units[$unit] = $info['name'];
    }

    $form['units'] = array(
      '#type' => 'select',
      '#title' => t('Units'),
      '#default_value' => $this->options['units'],
      '#options' => $units,
    );

    $form['show_units'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show units'),
      '#default_value' => $this->options['show_units'],
    );
  }

  function render($values) {
    $order_id = $this->get_value($values);
    $order = commerce_order_load($order_id);
    $weight = commerce_physical_order_weight($order, $this->options['units']);
    if (!empty($this->options['show_units'])) {
      return physical_weight_format($weight);
    }
    return $weight['weight'];
  }
}
