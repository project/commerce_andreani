<?php

/**
 * Field handler to display an order e-mail address.
 */
class commerce_andreani_handler_field_order_dimensions extends views_handler_field {

  function option_definition() {
    $options = parent::option_definition();

    $options['dimension'] = array('default' => '');
    $options['units'] = array('default' => 'cm');
    $options['show_units'] = array('default' => FALSE);
    $options['multiple_products_text'] = array('default' => '');

    return $options;
  }

  /**
   * Provide the units and show_units options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $units = array();
    foreach (physical_dimension_units() as $unit => $info) {
      $units[$unit] = $info['name'];
    }

    $form['dimension'] = array(
      '#type' => 'select',
      '#title' => t('Dimension'),
      '#default_value' => $this->options['dimension'],
      '#options' => array(
        '' => t('- Please select -'),
        'length' => t('Length'),
        'width' => t('Width'),
        'height' => t('Height'),
      ),
    );

    $form['units'] = array(
      '#type' => 'select',
      '#title' => t('Units'),
      '#default_value' => $this->options['units'],
      '#options' => $units,
    );

    $form['show_units'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show units'),
      '#default_value' => $this->options['show_units'],
    );

    $form['multiple_products_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Multiple products text'),
      '#description' => t("When the order has more than one physical product we don't know the final package dimensions, so this text will be printed instead."),
      '#default_value' => $this->options['multiple_products_text'],
    );
  }

  function render($values) {
    $order_id = $this->get_value($values);
    $order = commerce_order_load($order_id);
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $unit = $this->options['units'];
    $dimension = $this->options['dimension'];

    // Loop over each line item on the order.
    $physical_products = 0;
    foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      // Get the dimensions value of product line items.
      if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
        $line_item_dimensions = commerce_physical_product_line_item_dimensions($line_item_wrapper->value());
        if (!physical_field_is_empty($line_item_dimensions, array('type' => 'physical_dimensions'))) {
          $converted_dimensions = physical_dimensions_convert($line_item_dimensions, $unit);

          // Stop iterating if there is more than one physical product.
          $physical_products += $line_item_wrapper->quantity->value();
          if ($physical_products > 1) {
            break;
          }
        }
      }
    }

    if ($physical_products == 1) {
      $value = round($converted_dimensions[$dimension]);
      if (!empty($this->options['show_units'])) {
        return $value . ' ' . physical_dimension_unit_abbreviation($converted_dimensions['unit']);
      }
      return $value;
    }

    // No physical products in the order.
    if ($physical_products == 0) {
      return 0;
    }

    // More than one physical products in the order.
    return $this->options['multiple_products_text'];
  }
}
