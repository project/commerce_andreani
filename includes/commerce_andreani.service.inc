<?php

/**
 * @file
 * Functions to interact with the Andreani webservice.
 */

use Andreani\Andreani;
use Andreani\Requests\CotizarEnvio;
use Andreani\Requests\ConfirmarCompra;
use Andreani\Requests\ConsultarSucursales;

/**
 * Wrapper around the Andreani web service to use inside Drupal Commerce.
 */
class AndreaniWrapper {
  public function __construct($environment, $client, $user, $password) {
    $this->environment = $environment;
    $this->user = $user;
    $this->password = $password;
    $this->client = $client;
    $this->service = new Andreani($user, $password, $environment);
  }

  /**
   * Sets the weight of the products being shipped.
   *
   * @param $weight
   *   commerce physical weight array (in g)
   */
  public function setWeight($weight) {
    if (!empty($weight['weight']) && $this->validUnits($weight)) {
      $this->weight = $weight;
      return $this;
    }
    throw new CommerceAndreaniException(t('Invalid weight'));
  }

  /**
   * Sets the volume of the products being shipped.
   *
   * @param $volume
   *   commerce physical volume array (in cm^3).
   */
  public function setVolume($volume) {
    if (!empty($volume['volume']) && $this->validUnits($volume)) {
      $this->volume = $volume;
      return $this;
    }
    throw new CommerceAndreaniException(t('Invalid volume'));
  }

  /**
   * Sets the value of the products being shipment.
   *
   * @param $value
   *   price in ARS (decimal)
   */
  public function setValue($value) {
    $this->value = $value;
    return $this;
  }

  /**
   * Sets the address of the shipment.
   *
   * @param $address
   *   addressfield array (with addressfield_ar format)
   */
  public function setAddress($address) {
    $this->address = $address;
    return $this;
  }

  /**
   * Sets the pickup store.
   *
   * @param $store
   *   store id as returned by AndreaniWrapper::getStores
   */
  public function setStore($store) {
    $store = intval(trim($store));
    if ($store) {
      $this->store = $store;
      return $this;
    }
    throw new CommerceAndreaniException(t('Invalid andreani store.'));
  }

  /**
   * Sets the weight, volume, value and address of the shipment form the order.
   *
   * @param $order
   *   the order being shipped
   */
  public function processOrder($order) {
    $this->setVolume(commerce_physical_order_volume($order, 'cm'));
    $this->setWeight(commerce_physical_order_weight($order, 'g'));
    $this->setValue(commerce_andreani_order_value($order));
    $this->setAddress(commerce_andreani_get_order_shipping_address($order));
    if (!$this->volume || !$this->weight || !$this->value) {
      throw new CommerceAndreaniException();
    }
    $this->order_number = $order->order_number;
    $this->mail = $order->mail;
    return $this;
  }

  /**
   * Rate shipping for $order using $contract service.
   *
   * @param $shipping_service
   *   the shipping service name
   */
  public function rate($shipping_service, $order = NULL) {
    if (!is_null($order)) {
      $this->processOrder($order);
    }
    $request = new CotizarEnvio();
    $request->setCodigoDeCliente($this->client);
    $request->setNumeroDeContrato($this->getContract($shipping_service));
    $request->setPeso($this->weight['weight']);
    $request->setVolumen($this->volume['volume']);
    $request->setValorDeclarado($this->value);
    if ($shipping_service == 'andreani_store_pickup' && isset($this->store)) {
      $request->setCodigoDeSucursal($this->store);
    }
    else {
      $request->setCodigoPostal($this->address['postal_code']);
    }

    $response = $this->call($request);
    return $response;
  }

  /**
   * Confirms shipment in Andreani.
   */
  public function confirm($shipping_service, $order = NULL) {
    if (!is_null($order)) {
      $this->processOrder($order);
    }
    $request = new ConfirmarCompra();

    // Address or pickup store.
    if ($shipping_service == 'andreani_store_pickup') {
      $request->setCodigoDeSucursal($this->store);
    }
    else {
      $request->setCodigoPostal($this->address['postal_code']);
      $request->setCalle($this->address['thoroughfare_name']);
      $request->setNumero($this->address['thoroughfare_number']);
      $province = $this->address['administrative_area'];
      if ($province == 'C') {
        $request->setLocalidad('C.A.B.A.');
      }
      else {
        module_load_include('inc', 'addressfield', 'addressfield.administrative_areas');
        $administrative_areas = addressfield_get_administrative_areas('AR');
        if (isset($administrative_areas[$province])) {
          $request->setProvincia($administrative_areas[$province]);
        }
        $request->setLocalidad($this->address['locality']);
      }
      if (!empty($this->address['premise_number'])) {
        $request->setPiso($this->address['premise_number']);
      }
      if (!empty($this->address['sub_premise_number'])) {
        $request->setDepartamento($this->address['sub_premise_number']);
      }
    }

    // Addressee.
    $request->setNombreYApellido($this->address['name_line']);
    $request->setEmail($this->mail);
    // Support for addressfield_phone.
    if (!empty($this->address['phone_number'])) {
      $request->setNumeroDeTelefono($this->address['phone_number']);
    }

    // Transaction.
    $request->setNumeroDeContrato($this->getContract($shipping_service));
    $request->setNumeroDeTransaccion($this->order_number);

    // Shipment.
    $request->setPeso($this->weight['weight']);
    $request->setVolumen($this->volume['volume']);
    $request->setValorDeclarado($this->value);

    $response = $this->call($request);
    if (!isset($response->NumeroAndreani)) {
      throw new CommerceAndreaniException(t("Error creating shipment in Andreani, we didn't get a NumeroAndreani"));
    }
    return array(
      'andreani_id' => $response->NumeroAndreani,
      'receipt' => !empty($response->Recibo) ? $response->Recibo : '',
    );
  }

  /**
   * Get list of Andreani stores indexed by province.
   *
   * @param $province
   *   province code (as in addressfield administrative area) (optional)
   */
  public function getStores($province = NULL) {
    $request = new ConsultarSucursales();
    // We need to map the province code to values that the Andreani web service
    // understands.
    $province = strtoupper($province);
    if ($province == 'C') {
      $request->setLocalidad('C.A.B.A.');
    }
    else {
      module_load_include('inc', 'addressfield', 'addressfield.administrative_areas');
      $administrative_areas = addressfield_get_administrative_areas('AR');
      if (isset($administrative_areas[$province])) {
        $request->setProvincia($administrative_areas[$province]);
      }
    }
    $response = $this->call($request);

    $stores = array();
    if (isset($response->ResultadoConsultarSucursales)) {
      foreach ($this->ensureArray($response->ResultadoConsultarSucursales) as $store) {
        $id = intval($store->Sucursal);
        $stores[$id] = array(
          'id' => $id,
          'name' => $store->Descripcion,
          'address' => $store->Direccion,
          'hours' => $store->HoradeTrabajo,
          'lat' => $store->Latitud,
          'lon' => $store->Longitud,
        );
      }
    }

    return $stores;
  }

  /**
   * Sends request to andreani.
   */
  protected function call($request) {
    $parts = explode('\\', get_class($request));
    $type = end($parts);
    $wrapper = $type . 'Result';
    $response = $this->service->call($request);
    if ($response->isValid()) {
      return $response->getMessage()->{$wrapper};
    }

    throw new CommerceAndreaniException($response->getMessage());
  }

  /**
   * Get the contract number for $shipping_service.
   */
  protected function getContract($shipping_service) {
    $contract_type = str_replace('andreani_', '', $shipping_service);
    $contracts = commerce_andreani_enabled_contracts();
    if (isset($contracts[$contract_type])) {
      return $contracts[$contract_type];
    }
    throw new CommerceAndreaniException(t('There is no contract for !type', array('!type' => $contract_type)));
  }

  /**
   * Ensures $response is an array and not an object.
   */
  protected function ensureArray($response) {
    if (is_object($response)) {
      $response = array($response);
    }
    elseif (is_null($response)) {
      $response = array();
    }

    return $response;
  }

  /**
   * Validates the units for weight or volume arrays.
   *
   * Andreani needs cm^3 for volume and g for weight.
   *
   * @param $value
   *   the volume or weight array to validate
   */
  protected function validUnits($value) {
    if (isset($value['unit'])) {
      // Weight.
      if (isset($value['weight'])) {
        return $value['unit'] == 'g';
      }

      // Volume.
      if (isset($value['volume'])) {
        return $value['unit'] == 'cm';
      }
    }

    return FALSE;
  }
}

/**
 * Exception class for commerce_andreani.
 */
class CommerceAndreaniException extends Exception {
}
