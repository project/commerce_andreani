<?php

/**
 * @file
 * Admin functions for Commerce etrans.
 */


/**
 * Form builder function for module settings.
 */
function commerce_andreani_settings_form() {
  drupal_add_css(drupal_get_path('module', 'commerce_andreani') . '/css/commerce_andreani.settings.css');
  $form = array();

  $form['commerce_andreani_environment'] = array(
    '#type' => 'select',
    '#title' => t('Environment'),
    '#description' => t('Use Staging for testing, no shipment will be created. When you are ready to start shipping and have the credentials switch to Production.'),
    '#options' => array(
      'test' => t('Staging'),
      'prod' => t('Production'),
    ),
    '#empty_value' => '',
    '#empty_option' => t('Please select'),
    '#default_value' => variable_get('commerce_andreani_environment', ''),
    '#required' => TRUE,
  );

  $form['credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Credentials'),
    '#description' => t('To use the production environment you need to enter your Andreani credentials, ask your account executive.'),
    '#tree' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_andreani_environment"]' => array('value' => 'prod'),
      ),
    ),
    '#collapsible' => TRUE,
  );

  $form['credentials']['commerce_andreani_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('commerce_andreani_username', ''),
    '#states' => array(
      'required' => array(
        ':input[name="commerce_andreani_environment"]' => array('value' => 'prod'),
      ),
    ),
  );

  $form['credentials']['commerce_andreani_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('commerce_andreani_password', ''),
    '#states' => array(
      'required' => array(
        ':input[name="commerce_andreani_environment"]' => array('value' => 'prod'),
      ),
    ),
  );

  $form['client_contracts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client and contracts'),
    '#description' => t("You should enter your client and contract numbers here in order to use the production environment. Ask your Andreani account manager if you don't have them."),
    '#tree' => FALSE,
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_andreani_environment"]' => array('value' => 'prod'),
      ),
    ),
    '#collapsible' => TRUE,
  );

  $form['client_contracts']['commerce_andreani_client'] = array(
    '#type' => 'textfield',
    '#title' => t('Client'),
    '#default_value' => variable_get('commerce_andreani_client', ''),
    '#states' => array(
      'required' => array(
        ':input[name="commerce_andreani_environment"]' => array('value' => 'prod'),
      ),
    ),
  );

  $form['client_contracts']['commerce_andreani_contracts'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );
  $contract_types = commerce_andreani_contract_types();
  $default_contracts = variable_get('commerce_andreani_contracts', array_fill_keys(array_keys($contract_types), ''));
  foreach ($contract_types as $type => $contract) {
    $description = t("Enter your contract number for Andreani !contract (i.e. !staging) or leave empty if you don't want your customers to be able to select this service.", array(
      '!staging' => $contract['staging contract'],
      '!contract' => $contract['title'],
    ));
    $form['client_contracts']['commerce_andreani_contracts'][$type] = array(
      '#type' => 'textfield',
      '#title' => t('Contract number for !contract', array('!contract' => $contract['title'])),
      '#description' => $contract['description'] . '<br />' . $description,
      '#default_value' => $default_contracts[$type],
    );
  }

  $form['sender'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sender information'),
    '#description' => t('Sender information for packages.'),
    '#tree' => FALSE,
    '#collapsible' => TRUE,
  );
  $form['sender']['commerce_andreani_sender_first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First name'),
    '#default_value' => variable_get_value('commerce_andreani_sender_first_name'),
    '#required' => TRUE,
  );
  $form['sender']['commerce_andreani_sender_last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#default_value' => variable_get_value('commerce_andreani_sender_last_name'),
    '#required' => TRUE,
  );
  $form['sender']['commerce_andreani_sender_document_number'] = array(
    '#type' => 'textfield',
    '#title' => t('DNI'),
    '#size' => 10,
    '#default_value' => variable_get_value('commerce_andreani_sender_document_number'),
    '#required' => TRUE,
  );
  $form['sender']['commerce_andreani_sender_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#default_value' => variable_get_value('commerce_andreani_sender_email'),
    '#required' => TRUE,
  );

  $form['sender']['commerce_andreani_sender_mobile_area_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Area code'),
    '#size' => 4,
    '#default_value' => variable_get_value('commerce_andreani_sender_mobile_area_code'),
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('area-code'),
      'placeholder' => '011',
    ),
  );
  $form['sender']['commerce_andreani_sender_mobile_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Mobile phone'),
    '#size' => 10,
    '#default_value' => variable_get_value('commerce_andreani_sender_mobile_number'),
    '#required' => TRUE,
    '#attributes' => array(
      'class' => array('phone-number'),
      'placeholder' => '15 9999 9999',
    ),
  );

  $form['#validate'][] = 'commerce_andreani_settings_form_validate';

  return system_settings_form($form);
}


/**
 * Andreani settings form: validate callback.
 */
function commerce_andreani_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['commerce_andreani_environment'] && $values['commerce_andreani_environment'] == 'prod') {
    $required_fields = array(
      'commerce_andreani_username' => t('Username'),
      'commerce_andreani_password' => t('Password'),
      'commerce_andreani_client' => t('Client'),
    );
    foreach ($required_fields as $field => $label) {
      if (empty($values[$field])) {
        form_set_error($field, t('!name field is required.', array('!name' => $label)));
      }
    }
  }
}
