core = 7.x
api = 2

libraries[andreani][type] = library
libraries[andreani][download][type] = git
libraries[andreani][download][url] = https://github.com/eandreani/sdk-php.git
libraries[andreani][directory_name] = andreani-php-sdk
