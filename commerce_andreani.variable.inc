<?php

/**
 * @file
 * Variable definitions.
 */

/**
 * Implements hook_variable_info().
 *
 * Expose Andreani sender information as variables to use inside views.
 */
function commerce_andreani_variable_info($options) {
  $variables = array();

  // Sender information.
  $variables['commerce_andreani_sender_first_name'] = array(
    'type' => 'string',
    'title' => t('First name', array(), $options),
    'default' => '',
    'required' => TRUE,
    'localize' => FALSE,
  );

  $variables['commerce_andreani_sender_last_name'] = array(
    'type' => 'string',
    'title' => t('Last name', array(), $options),
    'default' => '',
    'required' => TRUE,
    'localize' => FALSE,
  );

  $variables['commerce_andreani_sender_document_number'] = array(
    'type' => 'string',
    'title' => t('DNI', array(), $options),
    'default' => '',
    'required' => TRUE,
    'localize' => FALSE,
    'element' => array('#size' => 8),
  );

  $variables['commerce_andreani_sender_email'] = array(
    'type' => 'string',
    'title' => t('E-mail', array(), $options),
    'default' => '',
    'required' => TRUE,
    'localize' => FALSE,
  );

  $variables['commerce_andreani_sender_mobile_area_code'] = array(
    'type' => 'string',
    'title' => t('Area code', array(), $options),
    'default' => '',
    'required' => TRUE,
    'localize' => FALSE,
    'element' => array('#size' => 4),
  );

  $variables['commerce_andreani_sender_mobile_number'] = array(
    'type' => 'string',
    'title' => t('Mobile number', array(), $options),
    'default' => '',
    'required' => TRUE,
    'localize' => FALSE,
    'element' => array('#size' => 10),
  );

  return $variables;
}
